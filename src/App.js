import React from "react";
import Home from "./pages/Home.page";
import Favorites from "./pages/Favorites.page";
import NavBar from "./components/Navbar.component";
import { Routes, Route } from "react-router-dom";

const App = () => {
    return (
        <>
            <NavBar />
            <section className="pt-32 bg-gradient min-h-svh">
                <Routes>
                    <Route exact path="/" element={<Home />}></Route>
                    <Route exact path="/favorites" element={<Favorites />} />
                </Routes>
            </section>
        </>
    );
};

export default App;
