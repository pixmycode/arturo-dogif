import React, { useEffect, useState } from "react";
import CardsContainer from "../components/CardsContainer.component";

const Favorites = () => {
    const [gifs, setGifs] = useState(
        localStorage.getItem("favoriteGifIds")
            ? JSON.parse(localStorage.getItem("favoriteGifIds"))
            : localStorage.settem("favoriteGifIds", JSON.stringify([]))
    );

    useEffect(() => {
        console.log("useEffect");
        setGifs(JSON.parse(localStorage.getItem("favoriteGifIds")));
    }, []);

    return (
        <>
            <div className="text-container mx-28">
                <h1 className="text-center font-bold font-poppins text-3xl">
                    Favoritos
                </h1>
                <p className="text-left">Total guardados: {gifs.length}</p>
            </div>
            <CardsContainer gifs={gifs} />
        </>
    );
};

export default Favorites;
