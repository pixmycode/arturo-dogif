import logo from "../logo.svg";
import { Link } from "react-router-dom";

const NavBar = () => {
    return (
        <nav className="fixed top-0 w-screen p-5 m-0 flex bg-white items-center justify-center text-black z-50 rounded-b-3xl border-solid border-2 border-black">
            <Link to="/">
                <div className="flex justify-center items-center gap-2">
                    <img className="h-8" alt="logo" src={logo} />
                    <p className="m-0 text-lg uppercase tracking-wide font-bold	">
                        Dogif
                    </p>
                </div>
            </Link>
            <div className="ml-auto flex gap-4">
                <Link to="/favorites" className="text-md uppercase font-bold">
                    Favoritos
                </Link>
            </div>
        </nav>
    );
};

export default NavBar;
