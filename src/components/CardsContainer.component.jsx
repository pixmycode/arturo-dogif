import Card from "./Card.component";

const CardsContainer = ({ gifs }) => {
    return (
        <div className="text-container grid grid-cols-item gap-y-10 gap-x-6 max-w-screen-xl m-center p-10 pb-20">
            {gifs.map((itemData) => (
                <Card
                    url={itemData.images ? itemData.images.preview_gif.url : ""}
                    key={itemData.id ? itemData.id : itemData}
                    id={itemData.id ? itemData.id : itemData}
                />
            ))}
        </div>
    );
};
export default CardsContainer;
