module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    colors:{
      callToAction: '#5352ed',
      navBar: '#FF004D',
      card: '#ffffff',
      white: '#ffffff',
      black: '#2d2d2d'
    },
    fontFamily: {
      poppins: ['Poppins', 'sans-serif'],
      merriweather: ['Merriweather', 'sans-serif']
    },
    extend: {
      margin: {
        'center': '0 auto',
      },
      gridTemplateColumns: {
        'item': 'repeat(auto-fill, minmax(16rem, 1fr));',
      },
      backgroundImage: {
        'gradient': "linear-gradient(120deg, #a1c4fd 0%, #c2e9fb 100%);",
      },
    },
  },
  plugins: [],
}